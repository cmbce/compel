//----------------------------
// parameters
//----------------------------
var defaultIssue = "";
var defaultComment = "";
var miniaturePlaceHolder = "http://placehold.it/120x50"

//----------------------------
// init function
//----------------------------
$(document).ready(function(){
    // allow instant replication of classes
    // Prevent the backspace key from navigating back.
    confirmBackspaceNavigations();
    $(function(){
        var rx = /INPUT|SELECT|TEXTAREA/i;

        $(document).bind("keydown keypress", function(e){
            if( e.which == 8 ){ // 8 == backspace
                if(e.target.disabled || e.target.readOnly ){
                    e.preventDefault();
                }
            }else{
                if(e.ctrlKey && e.which === 83){ // Ctrl+S
                    saveGeneralData();
                    e.preventDefault();
                    return false;
                }
            }
        });
    });
    // initiateMainTable();
    var promListIds = GetFromServer("listIdsTable");
    promListIds.done(function(data){
        var json = eval('('+data+')');
        if(json['itemvalue']==null){
            insertStandardRow(undefined,undefined,false);
            window.scrollTo(0,0);
        }
    });
    
    $("#jsonUploadInput").change(function(){
        var files = document.getElementById('jsonUploadInput').files;
        var reader = new FileReader();

        var file  = files[0];

        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function(e) {
            var json = e.target.result;
            sendToServer("allData",json,"loadAllDataJsonFromServer","update");
            loadAllDataJson(json);
        };
        reader.onerror = function(e) {
            console.log("Failed to read file");
        };
    });
    $("#date").html(makeTimeStamp());
});
//----------------------------
// functions
//----------------------------
function loadAllDataJsonFromServer(id,value){
    loadAllDataJson(value,"server");
}
function loadAllDataJson(json,origin){
    allData = JSON.parse(json);
    // here should add a header filler: all the metadata in allData
    // should be inserted in the header in an editable way
    for(var key in allData){
        if(allData.hasOwnProperty(key)){
            if(key != "generalData"){
                $("#"+key).html(allData[key]);
                // the update is done globally but for future updates:
                sendToServer(key,allData[key],"html","save"); 
            }
        }
    }

    // make the main table
    initiateMainTable();
    currentLineId = "";
    currentLineNum = "";
    if(allData.hasOwnProperty("generalData")){
        generalData = allData.generalData;
        for(var i = 0; i<generalData.length; i++){
            insertStandardRow(i); // +1 for title line
            if(origin != "server"){
                sendToServer(generalData[i]["id"],JSON.stringify(generalData[i]),"insertRowFromServer");
            }
        }
        if(origin != "server"){
            var listIdsTable = getListIdsTable();
            sendToServer("listIdsTable",JSON.stringify(listIdsTable),"currentTableIdsFromServer","save");
        }
    }else{
        generalData = [];
    }
}
function confirmBackspaceNavigations () {
    // http://stackoverflow.com/a/22949859/2407309
    var backspaceIsPressed = false
        $(document).keydown(function(event){
            if (event.which == 8) {
                backspaceIsPressed = true
            }
        })
    $(document).keyup(function(event){
        if (event.which == 8) {
            backspaceIsPressed = false
        }
        if (event.which == 27) {//echap
            // synchronize the currently selected 
            $(document.activeElement).trigger('blur');
        }
    })
    $(window).on('beforeunload', function(){
        if (backspaceIsPressed) {
            backspaceIsPressed = false
        return "Are you sure you want to leave this page?"
        }
    })
} // confirmBackspaceNavigations

function initiateMainTable(){
    // var tmpl = document.getElementById('table-template').content;
    // $("#main-table").html(document.importNode(tmpl,true));
    var tmpl = "<table id='myTable' width='100%'>"+
    		"    <colgroup>"+
    		"    <col width='500' align='left'/>"+
    		"    <col width='1000' align='left'/>"+
    		"    <col width='125' align='left'/>"+
    		"    </colgroup>"+
    		"    <tr>"+
    		"        <th align='left'>Variable</th>"+ 
    		"        <th align='left'>Mécanismes/limites</th>"+
    		"        <th>Force de l'effet</th>"+
    		"    </tr>"+
    		"</table>";
    $("#main-table").html(tmpl);
}
function randomString(length){
    var N = length;
    var out = Array(N+1).join((Math.random().toString(36)+'00000000000000000').slice(2, 18)).slice(0, N);
    return out;
}
function insertRowFromServer(id,value){
    var item = JSON.parse(value);
    var index = getGeneralIndex(item["id"]);
    if(index<0){
        index = generalData.length; 
        generalData[index] = {};
        for(var part in item){
            generalData[index][part] = item[part];
        }
        insertStandardRow(index,id);
        $("#issue_"+id).trigger('blur');
    }else{
        // ignore it, just comming back from same instance
    }
}
function currentTableIdsFromServer(id,val){
    // if any new id, add it
    var rowIds = JSON.parse(val);
    var listIdsTable = getListIdsTable();
    var iId = -1;
    (function getNextIfExist(iId){
        if(iId < rowIds.length-1){
            iId = iId +1; 
            var id = rowIds[iId];
            if($(id).not(listIdsTable).length < 1){
                var promLine = GetFromServer(id);
                promLine.always(function(){
                    getNextIfExist(iId);
                });
            }else{
                getNextIfExist(iId);
            }
        }
    })(iId);
}

function getListIdsTable(){
    var listIdsTable=[];
    for(var i = 0; i< generalData.length; i++){
        listIdsTable.push(generalData[i]["id"]);
    }
    return(listIdsTable);
}
// insert either a default row or the lineId row from generalData
function insertStandardRow(index,id,focus) {
    if(id === undefined){
        id = randomString(7);
    }
    if(focus === undefined){
        focus = true;
    }
    // if needed create the data structure 
    if(index === undefined){
        index = generalData.length; 
        generalData[index] = {};
        generalData[index]["id"] = id;
        generalData[index]["issue"] = defaultIssue;
        generalData[index]["comment"] = defaultComment;
        generalData[index]["rouletteData"] = new rouletteDataBase();
        generalData[index]["parameters"] = defaultParameters;
        // send data to server
        sendToServer(generalData[index]["id"],JSON.stringify(generalData[index]),"insertRowFromServer");

        // update the list of ids
        var listIdsTable = getListIdsTable();
        sendToServer("listIdsTable",JSON.stringify(listIdsTable),"currentTableIdsFromServer","save");
    }
    // add the data to the table
    var currentData = generalData[index];
    var table = document.getElementById("myTable");

    var row = table.insertRow(index+1);// +1 for title line
    row.id = "row"+currentData.id;

    makeEditableCell(row,0,"issue",currentData);
    makeEditableCell(row,1,"comment",currentData);

    var cell3 = row.insertCell(2);
    // cell3.innerHTML = '<div class="miniature" id="miniature_'+currentData.id+'" width="150" height="50"><img src=""></div>';
    cell3.innerHTML = '<div class="miniature" id="miniature_'+currentData.id+'"></div>';
    cell3.innerHTML += '<div class="legend" id="legend_miniature_'+currentData.id+'"></div>';
    $("#miniature_"+currentData.id).click(function(){
        if("miniature_"+currentLineId != this.id){
            editGraph(this.id);
        }
    });

    // set the focus of the roulette 
    editGraph(currentData.id);
    // if(focus){
    //     document.getElementById("issue_"+currentData.id).focus();
    // }
}

function makeEditableCell(row,index,type,currentData){
    var cell = row.insertCell(index);
    cell.className = "tdMainTable";
    var issueId = type+"_"+currentData.id;
    cell.innerHTML = '<textarea style="width:98%;height:98%" id="'+issueId+'">'+currentData[type]+'</textarea>';
    $("#"+issueId).blur(function(){
        var currentEditable = $("#"+issueId).attr("contentEditable");
        if(currentEditable == "true"){
            updateGeneralData(this.id,"table");
        }
    });
    $("#"+issueId).focus(function(){
        InEditableContent(this.id);
    });

    // flag as editable 
    $("#"+issueId).attr("contentEditable","true");
    $("#"+issueId).attr("readonly",false)

}
function setRouletteFocus(index){
    var currentData = generalData[index];
    currentLineId = generalData[index]["id"];
    currentLineNum = index;
    parameters = currentData.parameters;

    setUpAll(parameters);
    rouletteData = currentData.rouletteData; 
    buildRoulette();
    makeMiniature(currentData.id,focus=true);
}
// hook for msgTreatment
function generalDataServerUpdate(itemId,itemValue){
    $("#"+itemId).val(itemValue);
    updateGeneralData(itemId,"server");
}
function pit(){
    // do nothing
}

function updateGeneralData(idFrom,origin){
    // who's who
    var type = idFrom.split("_")[0];
    var idLine = idFrom.split("_")[1];
    
    // updates from bottom
    if(origin == "bottom"){
        idLine = currentLineId;
    }
    // get the index
    var index = getGeneralIndex(idLine);

    // update the database
    if(index<0){
        // forget about it, not yet opened that
        return false;
    }
    generalData[index][type] = $("#"+idFrom).val();

    // update the mirrors of the database
    var itemId = type+"_"+idLine;
    if(origin != "table" && origin != "server"){ 
        // update the table, not if server as server uses the table as the support for the update
        $("#"+itemId).val(generalData[index][type]);
    }
    if(origin != "server"){
        // server update
        sendToServer(itemId,generalData[index][type],"generalDataServerUpdate");
        sendToServer(idLine,JSON.stringify(generalData[index]),"insertRowFromServer","save");
    }
    if(origin != "bottom" && idLine == currentLineId){
        // update bottom
        $("#"+type+"_current").val(generalData[index][type]);
    }
    if(colorEditable){
        // colors and editability handling
        sendToServer(itemId+'-Editable',initEditable,"toggleEditable");
        changeIfColorSet(itemId,"black");
    }
}
function setMirror(className){
    $("."+className).change(function() {
        $('.'+$(this).attr('class')).val($(this).val());
    });
}
function editGraph(id){
    // save current
    if(currentLineId != ""){
        makeMiniature(currentLineId);
    }
    
    // update the roulette
    var newLineId = id.replace("miniature_","");
    var index = getGeneralIndex(newLineId);
    setRouletteFocus(index);
    $("#issue_current").val(generalData[index].issue);
    $("#issue_current").blur(function(){
        updateGeneralData(this.id,"bottom");
    });

    $("#comment_current").val(generalData[index].comment);
    $("#comment_current").blur(function(){
        updateGeneralData(this.id,"bottom");
    });

    // document.getElementById("elicitinggraph").focus();
}
// search and load the data with this id
function getGeneralIndex(id){
    for(var i = 0; i< generalData.length; i++){
        if(generalData[i].id == id){
            return i;
        }
    }
    return -1;
}
function makeTimeStamp(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
            dd='0'+dd
    } 

    if(mm<10) {
            mm='0'+mm
    } 

    timeStamp = yyyy+'-'+mm+'-'+dd;
    return timeStamp ;
}
function fillAllData(main){
    if(allData[main] === undefined){
        allData[main] = {};
    }
    var value = $("#"+main).html();
    allData[main] = value;
    return(value);
}
function saveGeneralData(){
    // update allData and get info for file name 
    var prototype = fillAllData("shortname");
    var expert = fillAllData("expert");
    var subject = fillAllData("subject");
    var date = fillAllData("date");

    // just update allData
    fillAllData("facilitator");
    fillAllData("introduction");
    fillAllData("question");
    allData["sessionid"]=sessionid;
    allData.generalData = generalData;

    // make JSON
    var out = [];
    out[0] = JSON.stringify(allData);
    var toSave = new Blob(out,{type:'text/html'});

    // var fileName = $.grep([date,prototype,subject,expert], Boolean).join("_")+".json";
    var fileName = [date,prototype,subject,expert].join("_")+".json";
    var fileName2 = fileName.replace(" ","-");
    saveAs(toSave,fileName2);
}
