<?php 
// determine sessionid and if none defined, recall with a new session id
// reads something like 
// http://192.168.33.13/public/CompEl/index.php?sessionid=59c1020a4c8a9&lang=fr
$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';

$sessionid = isset($_GET['sessionid']) ? $_GET['sessionid'] : '';
if($sessionid == ""){
    $sessionid = uniqid();
    header("Location: ".$_SERVER['PHP_SELF']."?sessionid=".$sessionid."&lang=".$lang);
}
// echo 'sessionid:'.$sessionid;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//FR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang;?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link rel="shortcut icon" href="../favicons/favicon.ico" sizes="32x32">
        <title>CompEl</title>

        <link rel="stylesheet" href="./CompEl_files/jquery-ui-1.8.9.custom.css"/>
        <link rel="stylesheet" href="./CompEl_files/uncertainty.css"/>
        <script src="./CompEl_files/FileSaver.min.js" type="text/javascript"></script> <!-- for saveAs -->

        <script type="text/javascript" src="./CompEl_files/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="./CompEl_files/jquery-ui.min.js"></script>
        <script type="text/javascript">
var url = [location.protocol, '//', location.host, location.pathname].join('');
var sessionid = "<?php echo $sessionid; ?>";
var lang = "<?php echo $lang; ?>";
var serverAvailable = true;
        </script>
        <script type="text/javascript" src="simpleexchange/simpleExchangeAPI.js"></script>

        <!--[if lte IE 8]>
            <script type="text/javascript" src="flotie78/jquery.flot.js"></script>
        <![endif]-->
        <!--[if gt IE 8]><!-->
            <script type="text/javascript" src="./CompEl_files/jquery.flot.js"></script>
            <!--<![endif]-->

            <script type="text/javascript" src="./CompEl_files/jquery.blockUI.js"></script> <!-- for element blocking -->
            <script type="text/javascript" src="./CompEl_files/uncertainty.min.js"></script> 
            <script type="text/javascript" src="./CompEl_files/resultTable.js"></script> 
            <!--[if IE]>
                <script type="text/javascript" src="json2.min.js"></script>
            <![endif]-->

            <!--[if lte IE 8]>
                <script type="text/javascript" src="flot/excanvas.js"></script>
            <![endif]-->

    </head>
    <body class="hasGoogleVoiceExt">

        <?php 
          // translation easy switch
          // Allows to use : 
          $translate = json_decode(file_get_contents("CompEl_files/language.json"), TRUE);
          function L($id) {
          global $translate,$lang;
          echo $translate[$id][$lang];
          }
          ?>
          <div id="generalTitle" style="width:500px; margin:0 auto;">
              <h1 align="center">CompEl</h1>
              <h3 align="center"><?php L("subTitle")?></h3>
          </div>
          <div class="demo" style="clear:both;">
              <div id="topcontainer">
                  <div align="center" id="toolbar">
                      <div id="innertoolbar" style="width:550px;margin:auto;">
                          <a id="helpbutton" class="ui-widget-header dialogbutton" href="#">
                              <?php L("help")?>
                          </a>
                          <a id="aboutbutton" class="ui-widget-header dialogbutton" href="#">
                              <?php L("aboutUs")?>
                          </a>

                          <a class="ui-widget-header dialogbutton" href="." onclick="window.open(url+'?lang=<?php echo $lang;?>'); return false;"><?php L("openSession")?> </a>
                          <a class="ui-widget-header dialogbutton" href="." onclick="window.open(url+'?sessionid='+sessionid+'&lang=en','_self'); return false;">En </a>
                          <a class="ui-widget-header dialogbutton" href="." onclick="window.open(url+'?sessionid='+sessionid+'&lang=fr','_self'); return false;">Fr </a><br/>
                      </div>
                  </div>
              </div>
          </div>

          <div id="all" style="float:left;">
              <div id="headerquestionnaire" style="width:100%;float:left;">
                  <div style="float:left">
                      <span class="titleheader">Questionnaire : </span><span id="shortname" class="entryheader shareable"><?php L("shortQuestName")?></span><br/>
                      <input type="file" id="jsonUploadInput"/><br/>
                      <br/>
                  </div>
              </div>
          </div>
          <div style="float:right;">
              <span class="titleheader"><?php L("date")?></span><span id="date" class="entryheader shareable">""</span><br/>
              <span class="titleheader"><?php L("expert")?></span><span id="expert" class="entryheader shareable" ><?php L("exName")?></span><br/>
              <span class="titleheader"><?php L("FacilitatorName")?></span><span id="facilitator" class="entryheader shareable" ><?php L("exName")?></span><br/>
              <span class="titleheader"><?php L("subject")?>:</span><span id="subject" class="entryheader shareable"><?php L("subject")?></span><br/>
          </div>
          <div id="introduction" style="float:left;max-width=600px;clear:both;" class="shareable">
              <h1>Introduction</h1>
              <?php L("textIntro")?> 
          </div>

          <div id="example" style="float:left;max-width=600px;clear:both;" class="shareable">
              <h2><?php L("exampleRelQuant")?></h2>
              <?php L("textExampleRelQuant")?> <br/>
              <img src="CompEl_files/images/correlationPlotExampleXY.png" width="577" style="padding-left:20px;padding-right:3px" alt="Example of Strong Correlation"/>
          </div>
          <div id="descriptionY" style="float:left;max-width=600px;clear:both;" class="shareable">
              <h2><?php L("relevantXY")?></h2>
              <?php L("textRelevantXY")?>
          </div>

          <div id="bodyquestionnaire" style="padding-top:20px;clear:both">
              <div id="titlequestionnaire" class="shareable">
                  <h1>Questionnaire</h1>

              <span class="titleheader"><?php L("questionTag")?></span><span id="question" class="entryheader shareable" style="min-height:3em;"><?php L("formulationQuestion")?></span><br/>

              </div>
          </div>
              <div id="main-table" style="padding-top:20px;">
                  <table id="myTable" width="100%">
                      <colgroup>
                          <col width="500" align="left"/>
                          <col width="1000" align="left"/>
                          <col width="125" align="left"/>
                      </colgroup>
                      <tr>
                          <th align="left"><?php L("variableCol")?></th>
                          <th align="left"><?php L("mecaCol")?></th>
                          <th align="center"><?php L("quantiCol")?></th>
                      </tr>
                  </table>
              </div>
              <br/>
              <div style="float:right">
                  <button onclick="insertStandardRow()" tabindex="0"><?php L("addRow")?></button>
                  <button onclick="saveGeneralData()" tabindex="0"><?php L("saveAsJson")?></button>
              </div>

          <div id="elicitationspace" style="margin-top:2cm;width:900px" >
              <div id="roulette" class="elicitation" style="display:none;margin:auto;">
                  <div id="currentinfo" style="float:left;width:280px">
                      <table id="currentItemTable">
                          <colgroup>
                              <col width="270" align="left"/>
                          </colgroup>
              <tr><td><strong><?php L("variableCol")?></strong>
                      <textarea id="issue_current" style="width:270px;height:2.3em"></textarea>
                  </td></tr>
                  <tr><td><strong><?php L("mecaCol")?></strong><br/>
                          <textarea id="comment_current" style="width:270px;height:280px"></textarea>
                          <button type="button" id="savebutton" class="ui-widget-header dialogbutton">
                              <?php L("back")?>
                              <span class="buttonicons ui-icon ui-icon-zoomout"></span>
                          </button>
                      </td></tr>

                      </table>
                  </div>

                  <div id="elicitinggraph" tabindex="0" style="float:left;">
                      <div style="padding-left:20px;padding-bottom:0px"><strong> <?php L("quantiCol")?></strong></div>
                      <div id="roulettetable" ></div>
                      <div id="referencegraph" style="float:right;"></div> 
                      <img src="CompEl_files/images/correlationPlots.png" width="592" style="padding-left:5px;padding-right:3px;margin:auto;" alt="visual scale of correlation"/>
                      <div id="roulettechipcount">
                          <?php L("numberOfChips")?> = <span id="roulettechipcountnumber">0</span>
                      </div>

                  </div>
              </div>
          </div>
          </div>
          <div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-aboutus" style="display: none; z-index: 1002; outline: 0px; position: absolute; height: auto; width: 586px; top: 40px; left: 10px;">
              <div id="aboutus" class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 0px; height: 276.04px;">
                  <p>Welcome to CompEl, the Comparative Elicitation Tool. 
                  This tool was produced by <a href="https://www6.versailles-grignon.inra.fr/agronomie/CV/Barbu">Corentin Barbu</a>
                  with inspiration and reused code from the excellent <a href="http://optics.eee.nottingham.ac.uk/match/uncertainty.php">MATCH</a> tool by
                  Ed Morris and Jeremy Oakley.
                  </p>
              </div>
              <div id="instructions" class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 0px; height: 277.04px;">
                  <div id="elicitationoptionshelp" class="helppertinent">
                      <h2>How to use the questionnaire?</h2>
                      <h3> To create a questionnaire </h3>
                      <ol>
                          <li>Choose a ShortQuestionnaireName (top left: this is used in the name of the downloaded questionnaire name. </li> 
                          <li>Fill the introductory part : What is the purpose of the interview? What is exactly quantified? To edit just click on what you want to change.</li>
                          <li>
                              Add as many lines as needed, one per elicited relationship.
                          </li>
                          <li>
                              When you are done, click "Save as Json" to download your questionnaire.
                          </li>
                      </ol>
                      <h3> To fill the questionnaire </h3>
                      <ol>
                          <li> Share the address of the page (should be looking like http://server/CompEl/index.php?sessionid=59c0e7a05ea2b) with your expert(s).</li>
                          <li> Fill the expert/interview information (top right)</li>
                          <li> Note: you can open different sessions at the same time, simply load again the page in an other tab.</li>
                      </ol>

                      Then for each line:
                      <ol>
                          <li>
                              Describe how the response variable is connected to this specific item.
                          </li>
                          <li>
                              Fill the quantitative estimation
                          </li>
                          <li>
                              Compare your quantitative estimation to the above quantifications, specifically the strongest positive and strongest negative quantifications above
                          </li>
                      </ol>
                      </li>
                      <li>
                          When you are done, click "Save as Json" to download your answers.
                      </li>
                      </ol>
                  </div>

                  <div id="roulettemethodhelp" class="inputmethodhelp helpnonpertinent">
                      <h2>Roulette Method</h2>
                      <ul>
                          <li>Click on the grid to allocate chips to the marked bins. The probability of a value is represented by the proportion of chips allocated to that value.</li>
                          <li>
                              You may use any number of chips in total. We suggest at least 10.
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
    </body>
</html>
