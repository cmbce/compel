# CompEl: Visual comparison makes quantification easier #

Often the units of knowledge are more relative than absolute. Based on the concept of the roulette elicitation we facilitate here visual comparison between the values elicited to maximize the relative exactitude of the values. The MATCH elicitation tool, great to elicit one, absolute, value inspired this project and portions of the javascript code have been reused.  


## What to use for what ##

* CompEl.html : open the questionnary (empty) as a local file, no need for php server
* index.php : needs a php server, allows multiple openning of a same questionnary (just share the http address)

Once the page is open in your browser, you can edit the introduction and metadata, add rows, etc. and save the result as json. To reopen your questionnary later, just load the json file. When opened from a server you can also just reopen the same address in your browser (including the sessionid). This last option is *not* guaranteed to work though on the long term (over 1 day) as it depends on your redis persistency settings. 

The json output can be imported and analyzed in R using [compelexplorer](https://bitbucket.org/cmbce/r-package-compelexplorer/src/master/).

## How do I get set up? ##

###�For local use (on your computer) ###

* [Download](https://bitbucket.org/correlmetriquepaysageetbioag/compel/downloads) or clone this repository
* Just open CompEl.html in browser, double click on it should do it

###�For server use (access of multiple people on the same questionnary)
This is based on [simpleExchange](https://bitbucket.org/cmbce/simpleexchange), a php/redis/ajax package for instant synchronisation of visualized page for multiple users. Please follow installation directions for simpleExchange. Then for "conference use", open the index.php page and share the address that appears in your browser to share editing. 

## Contributions and feedbacks�##
* Contributions are welcome, please contact me and issue a pull request. 
* Your are more than welcome to share feedbacks on the issue tracker or directly using corentin/adot/barbu/at/grignon/dot/inra/dot/fr